
from __future__ import  division, print_function, unicode_literals
import numpy as np
import matplotlib.pyplot as plt
import random

X = np.array([[147, 150, 153, 158, 163, 165, 168, 170, 173, 175, 178, 180, 183]]).T
y = np.array([[ 49, 50, 51,  54, 58, 59, 60, 62, 63, 64, 66, 67, 68]]).T

# graph 1 nói về số liệu được cho
plt.title('Graph 1')
plt.plot(X, y, 'ro')
plt.xlabel('x')
plt.ylabel('y')
plt.axis([140, 200, 40 ,80])
plt.show()

# tìm w
# sao cho 1/2 ||y - Xbarw||2 2 min

one = np.ones((X.shape[0], 1))
Xbar = np.concatenate((one, X), axis = 1)

A = np.dot(Xbar.T, Xbar)
b = np.dot(Xbar.T, y)

w = np.dot(np.linalg.pinv(A), b)

print("w: " + str(w))

Xpoints = np.linspace(140, 185, 2)
Ypoints = Xpoints * w[1] + w[0]

# graph 2 đưa ra dự đoán cho 2 điêm thuộc Xpoints ( tương úng có y thuộc Ypoints )
# 2 điểm đánh dấu "*"
plt.title("Graph 2")
plt.plot(X, y, 'ro')
plt.xlabel('x')
plt.ylabel('y')
plt.plot(Xpoints, Ypoints, 'k', marker = '*') # đương thẳng được dự đoán
plt.show()



